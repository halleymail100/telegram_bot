package org.example;


import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.meta.api.methods.AnswerCallbackQuery;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


public class Bot extends TelegramLongPollingBot {


    public Bot() {
        super("6728265116:AAEhhcPlzPxQILNliMrDr0AUGqSc9JQmLTo");
        
    }

    public void onUpdateReceived(Update update) {

        if(update.hasCallbackQuery()){
            CallbackQuery callbackQuery = update.getCallbackQuery();
            String callback_query_id = callbackQuery.getId();
            String callback_query_data = callbackQuery.getData();
            String user = callbackQuery.getFrom().getUserName();
            String chat_id = callbackQuery.getMessage().getChatId().toString();

            
                System.out.println("CallbackQuery received:");
                System.out.println("ID: " + callback_query_id);
                System.out.println("From: " + user);
                System.out.println("Data: " + callback_query_data);

                handleCallbackQuery(callback_query_id, callback_query_data, chat_id);
            
               
        }

                //            Database variables
                String firstName = update.getMessage().getChat().getFirstName();
                String lastName = update.getMessage().getChat().getLastName();
        String user_id = update.getMessage().getChat().getId().toString();
        String username = update.getMessage().getChat().getUserName();
        String connectionString = "mongodb+srv://mongo:mongo@cluster0.ffmj4bp.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0";
        String dbName = "iuget";
        String collectionName = "users";

       
        
        if(update.hasMessage() && update.getMessage().hasText())
            {
                String userInput = update.getMessage().getText();
                String chat_id = update.getMessage().getChatId().toString();
                log(firstName, lastName, user_id, username, userInput);

                if(userInput.equals("/start") || userInput.equals("start"))
                {

                    // Send the welcome message to the user
            SendMessage message = new SendMessage();
            message.setChatId(chat_id);
            message.setText("Bienvenue! "+firstName+ " "+lastName+"\n\nPlease choose a prefered languange / Veuillez choisir votre langue ");
            InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
            List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
            rowsInline.add(Arrays.asList(
                InlineKeyboardButton.builder().text("French").callbackData("french").build(),
                InlineKeyboardButton.builder().text("English").callbackData("english").build()
            ));
            markupInline.setKeyboard(rowsInline);
            message.setReplyMarkup(markupInline);
            try{
                execute(message);
            }catch(TelegramApiException e){
                e.printStackTrace();
            }

                    System.out.println("Message sent successfully with language selection");
                }

                // Store the user update in the database
                    // Database database = new Database(connectionString, dbName, collectionName);
                    // database.saveUserUpdate(update);
                    // System.out.println("User update saved in the database");

            }



    }

    private void handleCallbackQuery(String callback_query_id, String callback_query_data, String chat_id) {
        // Handle the callback query
        System.out.println("Handling callBackquery:");
        System.out.println("ID: " + callback_query_id);
        System.out.println("Data: " + callback_query_data);
        System.out.println("Chat ID: " + chat_id);

        switch (callback_query_data) {
           

            case "french":
                System.out.println("French selected");
                SendMessage frmessage = new SendMessage();
                frmessage.setChatId(chat_id);
                InlineKeyboardMarkup frmarkupInline = new InlineKeyboardMarkup();
                List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
                rowsInline.add(Arrays.asList(
                    InlineKeyboardButton.builder().text("Bonamoussadi").callbackData("frbonamoussadi").build(),
                    InlineKeyboardButton.builder().text("Bonaberi").callbackData("frbonaberi").build()
                ));
                frmarkupInline.setKeyboard(rowsInline);
                frmessage.setReplyMarkup(frmarkupInline);
                try{
                    execute(frmessage);
                }catch(TelegramApiException e){
                    e.printStackTrace();
                }
                
                AnswerCallbackQuery answerCallbackQuery = new AnswerCallbackQuery();
                answerCallbackQuery.setCallbackQueryId(callback_query_id);
                answerCallbackQuery.setText("Vous avez choisi la langue française, Vieller choisir votre campus");
                try{
                    execute(answerCallbackQuery);
                }catch(TelegramApiException e){
                    e.printStackTrace();
                }
                break;

                case "english":
                System.out.println("English selected");
                SendMessage engmessage = new SendMessage();
                engmessage.setChatId(chat_id);
                InlineKeyboardMarkup engmarkupInline = new InlineKeyboardMarkup();
                List<List<InlineKeyboardButton>> engRowsInline = new ArrayList<>();
                engRowsInline.add(Arrays.asList(
                    InlineKeyboardButton.builder().text("Bonamoussadi").callbackData("enbonamoussadi").build(),
                    InlineKeyboardButton.builder().text("Bonaberi").callbackData("enbonaberi").build()
                ));
                engmarkupInline.setKeyboard(engRowsInline);
                engmessage.setReplyMarkup(engmarkupInline);
                try{
                    execute(engmessage);
                }catch(TelegramApiException e){
                    e.printStackTrace();
                }

                AnswerCallbackQuery enganswerCallbackQuery = new AnswerCallbackQuery();
                enganswerCallbackQuery.setCallbackQueryId(callback_query_id);
                enganswerCallbackQuery.setText("You have selected the English language, please choose your campus");
                try{
                    execute(enganswerCallbackQuery);
                }catch(TelegramApiException e){
                    e.printStackTrace();
                }
                break;

                case "frbonamoussadi":
                System.out.println("frBonamoussadi selected");
                SendMessage bonamoussadiMessage = new SendMessage();
                bonamoussadiMessage.setChatId(chat_id);
                InlineKeyboardMarkup frbonamoussadiMarkupInline = new InlineKeyboardMarkup();
                List<List<InlineKeyboardButton>> frbonamoussadiRowsInline = new ArrayList<>();
                frbonamoussadiRowsInline.add(Arrays.asList(
                    InlineKeyboardButton.builder().text("Programme Anglophone").callbackData("frbonamoussadianglo").build()
                ));
                frbonamoussadiRowsInline.add(Arrays.asList(
                    InlineKeyboardButton.builder().text("Programme Francophone").callbackData("frbonamoussadifran").build()
                ));
                frbonamoussadiMarkupInline.setKeyboard(frbonamoussadiRowsInline);
                bonamoussadiMessage.setReplyMarkup(frbonamoussadiMarkupInline);
                try{
                    execute(bonamoussadiMessage);
                }catch(TelegramApiException e){
                    e.printStackTrace();
                }

                AnswerCallbackQuery frbonamoussadianswerCallbackQuery = new AnswerCallbackQuery();
                frbonamoussadianswerCallbackQuery.setCallbackQueryId(callback_query_id);
                frbonamoussadianswerCallbackQuery.setText("Vous avez choisi le campus de Bonamoussadi, veuillez choisir votre programme");
                try{
                    execute(frbonamoussadianswerCallbackQuery);
                }catch(TelegramApiException e){
                    e.printStackTrace();
                }
                break;

                case "frbonaberi":
                System.out.println("frBonaberi selected");
                SendMessage bonaberiMessage = new SendMessage();
                bonaberiMessage.setChatId(chat_id);
                InlineKeyboardMarkup frbonaberiMarkupInline = new InlineKeyboardMarkup();
                List<List<InlineKeyboardButton>> frbonaberiRowsInline = new ArrayList<>();
                frbonaberiRowsInline.add(Arrays.asList(
                    InlineKeyboardButton.builder().text("Programme Anglophone").callbackData("frbonaberianglo").build()
                ));
                frbonaberiRowsInline.add(Arrays.asList(
                    InlineKeyboardButton.builder().text("Programme Francophone").callbackData("frbonaberifran").build()
                ));
                frbonaberiMarkupInline.setKeyboard(frbonaberiRowsInline);
                bonaberiMessage.setReplyMarkup(frbonaberiMarkupInline);
                try{
                    execute(bonaberiMessage);
                }catch(TelegramApiException e){
                    e.printStackTrace();
                }

                AnswerCallbackQuery frbonaberianswerCallbackQuery = new AnswerCallbackQuery();
                frbonaberianswerCallbackQuery.setCallbackQueryId(callback_query_id);
                frbonaberianswerCallbackQuery.setText("Vous avez choisi le campus de Bonaberi, veuillez choisir votre programme");
                try{
                    execute(frbonaberianswerCallbackQuery);
                }catch(TelegramApiException e){
                    e.printStackTrace();
                }
                break;

                case "enbonamoussadi":
                System.out.println("enBonamoussadi selected");
                SendMessage enbonamoussadiMessage = new SendMessage();
                enbonamoussadiMessage.setChatId(chat_id);
                InlineKeyboardMarkup enbonamoussadiMarkupInline = new InlineKeyboardMarkup();
                List<List<InlineKeyboardButton>> enbonamoussadiRowsInline = new ArrayList<>();
                enbonamoussadiRowsInline.add(Arrays.asList(
                    InlineKeyboardButton.builder().text("Anglophone Program").callbackData("enbonamoussadianglo").build()
                ));
                enbonamoussadiRowsInline.add(Arrays.asList(
                    InlineKeyboardButton.builder().text("Francophone Program").callbackData("enbonamoussadifran").build()
                ));
                enbonamoussadiMarkupInline.setKeyboard(enbonamoussadiRowsInline);
                enbonamoussadiMessage.setReplyMarkup(enbonamoussadiMarkupInline);
                try{
                    execute(enbonamoussadiMessage);
                }catch(TelegramApiException e){
                    e.printStackTrace();
                }

                AnswerCallbackQuery enbonamoussadianswerCallbackQuery = new AnswerCallbackQuery();
                enbonamoussadianswerCallbackQuery.setCallbackQueryId(callback_query_id);
                enbonamoussadianswerCallbackQuery.setText("You have selected the Bonamoussadi campus, please choose your program");
                try{
                    execute(enbonamoussadianswerCallbackQuery);
                }catch(TelegramApiException e){
                    e.printStackTrace();
                }

                break;

                case "enbonaberi":
                System.out.println("enBonaberi selected");
                break;
        
            default:
                break;

                
        }
    }



    public String getBotUsername () {
        
        return "IUGET_bot";
    }

    @Override
    public String getBotToken () {
        
        return  "6728265116:AAEhhcPlzPxQILNliMrDr0AUGqSc9JQmLTo";
    }

    private void log(String first_name, String last_name, String user_id, String username, String userInput) {
        System.out.println("\n ----------------------------");
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date));
        System.out.println("Message from " + first_name + " " + last_name + "\nUser ID = " + user_id + "\nusername: " + username + "\nMessage: " + userInput);

    }


}


