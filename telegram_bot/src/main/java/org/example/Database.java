package org.example;


import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.telegram.telegrambots.meta.api.objects.Update;

public class Database {
    private MongoClient mongoClient;
    private MongoDatabase database;
    private MongoCollection<Document> collection;

    public Database(String connectionString, String dbName, String collectionName) {
// Establish a connection to MongoDB server
        mongoClient = MongoClients.create(connectionString);
// Connect to the database
        database = mongoClient.getDatabase(dbName);
// Connect to the collection
        collection = database.getCollection(collectionName);
    }

    public void saveUserUpdate(Update update) {
        if (update.hasMessage() && update.getMessage().hasText()) {
            Document userUpdate = new Document("chat_id", update.getMessage().getFrom().getId())
                    .append("first_name", update.getMessage().getFrom().getFirstName())
                    .append("last_name", update.getMessage().getFrom().getLastName())
                    .append("username", update.getMessage().getFrom().getUserName())
                    .append("message_text", update.getMessage().getText());
            collection.insertOne(userUpdate);
        }
    }

    // Close the connection when done
    public void close() {
        mongoClient.close();
    }
}