package org.example;

import com.theokanning.openai.service.OpenAiService;
import com.theokanning.openai.completion.CompletionRequest;

public class OpenAI {
    public  static final String API_KEY = "sk-proj-2UMF0XGrOmB96oi2XKbdT3BlbkFJmaKKcivosQI3ow69PtBm";
    public static  final String API_URL = "https://api.openai.com/v1/completions";

    private final OpenAiService service;

    public OpenAI(){
        this.service = new OpenAiService(API_KEY);
    }

    public String getOpenAIResponse(String prompt){
        CompletionRequest completionRequest = CompletionRequest.builder()
        .model("text-davinci-003")
        .prompt(prompt)
        .maxTokens(100)
        .build();

        String result = service.createCompletion(completionRequest).getChoices().get(0).getText();
        return result;
    }
}
